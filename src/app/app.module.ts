import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormGroup,  FormBuilder,  Validators } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LoginComponent } from './components/login/login.component';
import { RouterModule, Routes } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { TopbarleftmenuComponent } from './components/topbarleftmenu/topbarleftmenu.component';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { NgxPaginationModule } from 'ngx-pagination';
import { NgxSmartModalModule } from 'ngx-smart-modal';
import { ContactusComponent } from './components/contactus/contactus.component';
import { CalendarModule } from '@syncfusion/ej2-angular-calendars';
import { MyprofileComponent } from './components/myprofile/myprofile.component';
import { EditprofileComponent } from './components/editprofile/editprofile.component';
import { ChangepasswordComponent } from './components/changepassword/changepassword.component';
import { CategoriesComponent } from './components/categories/categories.component';
import { ArticlesComponent } from './components/articles/articles.component';
import { ParentalfiltersComponent } from './components/parentalfilters/parentalfilters.component';
import { SafearticlesComponent } from './components/safearticles/safearticles.component';
import { ViewparentalComponent } from './components/parentalfilters/viewparental/viewparental.component';
import { EditparentalComponent } from './components/parentalfilters/editparental/editparental.component';
import { AddparentalfilterComponent } from './components/parentalfilters/addparentalfilter/addparentalfilter.component';
import { UploadparentalcsvComponent } from './components/parentalfilters/uploadparentalcsv/uploadparentalcsv.component';
import { EditsafearticleComponent } from './components/safearticles/editsafearticle/editsafearticle.component';
import { AddsafearticleComponent } from './components/safearticles/addsafearticle/addsafearticle.component';
import { UploadsafearticlecsvComponent } from './components/safearticles/uploadsafearticlecsv/uploadsafearticlecsv.component';
import { BanArticlesComponent } from './components/ban-articles/ban-articles.component';
import { UploadBanArticleCSVComponent } from './components/ban-articles/upload-ban-article-csv/upload-ban-article-csv.component';
import { AddBanArticleComponent } from './components/ban-articles/add-ban-article/add-ban-article.component';
import { EditBanArticleComponent } from './components/ban-articles/edit-ban-article/edit-ban-article.component';
import { SubscriptiondetailComponent } from './components/subscriptiondetail/subscriptiondetail.component';
import { RecommendationengineComponent } from './components/recommendationengine/recommendationengine.component';
import { DatePickerModule } from '@syncfusion/ej2-angular-calendars';
import { EdituserdetailComponent } from './components/edituserdetail/edituserdetail.component';
import { OrderModule } from 'ngx-order-pipe';
import { UploadcssComponent } from './components/uploadcss/uploadcss.component';
import {LocationStrategy, HashLocationStrategy} from '@angular/common';



@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    DashboardComponent,
    TopbarleftmenuComponent,
    ContactusComponent,
    MyprofileComponent,
    EditprofileComponent,
    ChangepasswordComponent,
    CategoriesComponent,
    ArticlesComponent,
    ParentalfiltersComponent,
    SafearticlesComponent,
    ViewparentalComponent,
    EditparentalComponent,
    AddparentalfilterComponent,
    UploadparentalcsvComponent,
    EditsafearticleComponent,
    AddsafearticleComponent,
    UploadsafearticlecsvComponent,
    BanArticlesComponent,
    UploadBanArticleCSVComponent,
    AddBanArticleComponent,
    EditBanArticleComponent,
    SubscriptiondetailComponent,
    RecommendationengineComponent,
    EdituserdetailComponent,
    UploadcssComponent,
    
    
    
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    FormsModule,
    AngularFontAwesomeModule,
    NgxPaginationModule,
    NgxSmartModalModule.forRoot(),
    CalendarModule,
    DatePickerModule,
    OrderModule,
    
    

  ],
  providers: [{provide: LocationStrategy, useClass: HashLocationStrategy}
],
  bootstrap: [AppComponent]
})
export class AppModule { }


