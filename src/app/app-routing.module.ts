import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './components/login/login.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { ContactusComponent } from './components/contactus/contactus.component';
import { MyprofileComponent } from './components/myprofile/myprofile.component';
import { EditprofileComponent } from './components/editprofile/editprofile.component';
import { ChangepasswordComponent } from './components/changepassword/changepassword.component';
import { CategoriesComponent } from './components/categories/categories.component';
import { ArticlesComponent } from './components/articles/articles.component';
import { ParentalfiltersComponent } from './components/parentalfilters/parentalfilters.component';
import { ViewparentalComponent } from './components/parentalfilters/viewparental/viewparental.component';
import { EditparentalComponent } from './components/parentalfilters/editparental/editparental.component';
import { UploadparentalcsvComponent } from './components/parentalfilters/uploadparentalcsv/uploadparentalcsv.component';
import { AddparentalfilterComponent } from './components/parentalfilters/addparentalfilter/addparentalfilter.component';
import { SafearticlesComponent } from './components/safearticles/safearticles.component';
import { EditsafearticleComponent } from './components/safearticles/editsafearticle/editsafearticle.component';
import { AddsafearticleComponent } from './components/safearticles/addsafearticle/addsafearticle.component';
import { UploadsafearticlecsvComponent } from './components/safearticles/uploadsafearticlecsv/uploadsafearticlecsv.component';
import { BanArticlesComponent } from './components/ban-articles/ban-articles.component';
import { EditBanArticleComponent } from './components/ban-articles/edit-ban-article/edit-ban-article.component';
import { AddBanArticleComponent } from './components/ban-articles/add-ban-article/add-ban-article.component';
import { UploadBanArticleCSVComponent } from './components/ban-articles/upload-ban-article-csv/upload-ban-article-csv.component';
import { SubscriptiondetailComponent } from './components/subscriptiondetail/subscriptiondetail.component';
import { RecommendationengineComponent } from './components/recommendationengine/recommendationengine.component';
import { EdituserdetailComponent } from './components/edituserdetail/edituserdetail.component';
import { UploadcssComponent } from './components/uploadcss/uploadcss.component';


const routes: Routes = [
 {
    path: '',
    component: LoginComponent,
    pathMatch:'full'
  },
  {
    path: 'dashboard',
    component: DashboardComponent,
    pathMatch:'full'
  },
  {
    path: 'contactus',
    component: ContactusComponent,
    pathMatch:'full'
  },
  {
    path: 'myprofile',
    component: MyprofileComponent,
    pathMatch:'full'
  },
  {
    path: 'editprofile',
    component: EditprofileComponent,
    pathMatch:'full'
  },
  {
    path: 'changepassword',
    component: ChangepasswordComponent,
    pathMatch:'full'
  },
  {
    path: 'categories',
    component: CategoriesComponent,
    pathMatch:'full'
  },
  {
    path: 'articles/:id',
    component: ArticlesComponent,
    pathMatch:'full',
  },
  {
    path: 'parentalfilters',
    component: ParentalfiltersComponent,
    pathMatch:'full',
  },
  {
    path: 'parentalfilters/view',
    component: ViewparentalComponent,
    pathMatch:'full'
  },
  {
    path: 'parentalfilters/edit',
    component: EditparentalComponent,
    pathMatch:'full'
  },
  {
    path: 'parentalfilters/uploadcsv',
    component: UploadparentalcsvComponent,
    pathMatch:'full'
  },
 {
    path: 'parentalfilters/addNew',
    component: AddparentalfilterComponent,
    pathMatch:'full'
  },
  {
    path: 'safeArticles',
    component: SafearticlesComponent,
    pathMatch:'full'
  },
  
  {
    path: 'safeArticles/edit',
    component: EditsafearticleComponent,
    pathMatch:'full'
  },
  
  {
    path: 'safeArticles/addNew',
    component: AddsafearticleComponent,
    pathMatch:'full'
  },
  
  {
    path: 'safeArticles/uploadcsv',
    component: UploadsafearticlecsvComponent,
    pathMatch:'full'
  },
  {
    path: 'banArticles',
    component: BanArticlesComponent,
    pathMatch:'full'
  },
  {
    path: 'banArticles/edit',
    component: EditBanArticleComponent,
    pathMatch:'full'
  },
  {
    path: 'banArticles/addNew',
    component: AddBanArticleComponent,
    pathMatch:'full'
  },
  
  {
    path: 'banArticles/uploadcsv',
    component: UploadBanArticleCSVComponent,
    pathMatch:'full'
  },
  {
    path: 'subscriptionDetail',
    component: SubscriptiondetailComponent,
    pathMatch:'full'
  },
  {
    path: 'recommendationEngine',
    component: RecommendationengineComponent,
    pathMatch:'full'
  },
  {
    path: 'dashboard/editUserDetail',
    component: EdituserdetailComponent,
    pathMatch:'full'
  },
  {
    path: 'uploadCss',
    component: UploadcssComponent,
    pathMatch:'full'
  },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
