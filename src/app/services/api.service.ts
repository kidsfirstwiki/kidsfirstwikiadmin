import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders, HttpParams } from '@angular/common/http';
import * as moment from 'moment-timezone';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
 
  baseUrl= 'http://192.168.2.57:8001';
  //baseUrl= 'http://68.183.237.82:8000';
  constructor(private httpClient: HttpClient) {
	
  }
  
  httpHeaders = new HttpHeaders().set('Authorization',localStorage.getItem('token') );
  httpOptions = {
    headers: this.httpHeaders
  };   //specifically one step header go.. 

  
  getAdminProfile(){
    var token = localStorage.getItem("token");
    return this.httpClient.get<any>(this.baseUrl + '/getAdminProfile',{headers:{ 'Authorization':  token}});
  }

  editAdminProfile(customData:any){
    var token = localStorage.getItem("token");

  return this.httpClient.post<any>(this.baseUrl + '/editAdminProfile',customData,{headers:{ 'Authorization':  token}});
  }

  getCategoriesData(customData:any){
    var token = localStorage.getItem("token");
   	
    return this.httpClient.post<any>(this.baseUrl + '/getCategoriesData',customData,{headers:{ 'Authorization':  token}});
    }

  getArticlesByCategory(formData: any) {
    var token = localStorage.getItem("token");

    return this.httpClient.post<any>(this.baseUrl + '/getArticlesByCategory',formData,{headers:{ 'Authorization':  token}});
  }
  
  getParentalFiltersData(pageNo:any,maxRecords:any,filterTopic:any,checkTitle:any,checkBody:any,replaceWord:any){
    var token = localStorage.getItem("token");
    let httpParams = new HttpParams().set('pageNo', pageNo).set('maxRecords', maxRecords).set('filterTopic', filterTopic).set('checkTitle', checkTitle).set('checkBody', checkBody).set('replaceWord', replaceWord)
     return this.httpClient.get<any>(this.baseUrl + '/getParentalFilters', {headers: { 'Authorization':  token},params: httpParams});
   }
   
  
  editParentalFilters(customData:any){
    var token = localStorage.getItem("token");

    return this.httpClient.post<any>(this.baseUrl + '/editParentalFilters',customData,{headers:{ 'Authorization':  token}});
    }

  deleteParentalFilter(customData:any){
      var token = localStorage.getItem("token");
      return this.httpClient.post<any>(this.baseUrl + '/deleteParentalFilters',customData,{headers:{'Authorization':  token}});
      }

  uploadParentalCSVFile(customData:any){
    var token = localStorage.getItem("token");
    return this.httpClient.post<any>(this.baseUrl + '/uploadParentalCSVFile',customData,{headers:{ 'Authorization':  token}});
    }
  addParentalFilter(customData:any){
    var token = localStorage.getItem("token");

    return this.httpClient.post<any>(this.baseUrl + '/addParentalFilter',customData,{headers:{ 'Authorization':  token}});
    }

  getBaseUrl(){
    return this.baseUrl;
    }
  getSafeArticlesList(pageNo:any,maxRecords:any,searchText:any){
      var token = localStorage.getItem("token");

      let httpParams = new HttpParams().set('pageNo', pageNo).set('maxRecords', maxRecords).set('searchText', searchText)
       return this.httpClient.get<any>(this.baseUrl + '/getSafeArticlesList', {headers:{ 'Authorization':  token},params: httpParams});
     }
  deleteSafeArticle(customData:any){
      var token = localStorage.getItem("token");

      return this.httpClient.post<any>(this.baseUrl + '/deleteSafeArticle',customData,{headers:{ 'Authorization':  token}});
      }
    
  editSafeArticle(customData:any){
    var token = localStorage.getItem("token");

    return this.httpClient.post<any>(this.baseUrl + '/editSafeArticle',customData,{headers:{ 'Authorization':  token}});
    }
    
  addSafeArticle(customData:any){
      var token = localStorage.getItem("token");
      return this.httpClient.post<any>(this.baseUrl + '/addSafeArticle',customData,{headers:{ 'Authorization':  token}});
      }
    
  uploadSafeArticleCSV(customData:any){
    var token = localStorage.getItem("token");

    return this.httpClient.post<any>(this.baseUrl + '/uploadSafeArticleCSV',customData,{headers:{ 'Authorization':  token}});
    }
  deleteBanArticle(customData:any){
    var token = localStorage.getItem("token");

      return this.httpClient.post<any>(this.baseUrl + '/deleteBanArticle',customData,{headers:{ 'Authorization':  token}});
      }
    
  editBanArticle(customData:any){
    var token = localStorage.getItem("token");

    return this.httpClient.post<any>(this.baseUrl + '/editBanArticle',customData,{headers:{ 'Authorization':  token}});
    }
    
  addBanArticle(customData:any){
      var token = localStorage.getItem("token");

      return this.httpClient.post<any>(this.baseUrl + '/addBanArticle',customData,{headers:{ 'Authorization':  token}});
      }
    
  uploadBanArticleCSV(customData:any){
    var token = localStorage.getItem("token");

    return this.httpClient.post<any>(this.baseUrl + '/uploadBanArticleCSV',customData,{headers:{ 'Authorization':  token}});
    }

  getBanArticlesList(pageNo:any,maxRecords:any,searchText:any){
      var token = localStorage.getItem("token");

      let httpParams = new HttpParams().set('pageNo', pageNo).set('maxRecords', maxRecords).set('searchText', searchText)
       return this.httpClient.get<any>(this.baseUrl + '/getBanArticlesList', {headers: { 'Authorization':  token},params: httpParams});
     }
  changePassword(body:any){
      var token = localStorage.getItem("token");

      return this.httpClient.post<any>(this.baseUrl + '/changeAdminPassword',body,{headers:{ 'Authorization':  token}});
    }

  getRecommendationPercentage(){
      var token = localStorage.getItem("token");

      return this.httpClient.get<any>(this.baseUrl + '/getRecommendationPercentage',{headers:{ 'Authorization':  token}});
    }
    

  editRecommendationPercentage(customData:any){
    var token = localStorage.getItem("token");

    return this.httpClient.post<any>(this.baseUrl + '/editRecommendationPercentage',customData,{headers:{ 'Authorization':  token}});
    }

  editUserDetail(customData:any){
    var token = localStorage.getItem("token");

    return this.httpClient.post<any>(this.baseUrl + '/editUserDetail',customData,{headers:{ 'Authorization':  token}});
  }
  uploadCSS(customData:any){
    var token = localStorage.getItem("token");

    return this.httpClient.post<any>(this.baseUrl + '/uploadCSS',customData,{headers:{ 'Authorization':  token}});
    }
  
  exportCSV(){
    var token = localStorage.getItem("token");
    return this.httpClient.post<any>(this.baseUrl + '/exportCSVForBanTitles',{},{headers:{ 'Authorization':  token}});
  }

  exportCSVForSafeTitles(){
    var token = localStorage.getItem("token");
    return this.httpClient.post<any>(this.baseUrl + '/exportCSVForSafeTitles',{},{headers:{ 'Authorization':  token}});
  }
  exportCSVForParentalFilters(){
    var token = localStorage.getItem("token");
    return this.httpClient.post<any>(this.baseUrl + '/exportCSVForParentalFilters',{},{headers:{ 'Authorization':  token}});
  }

}