import { Component, OnInit } from '@angular/core';
import * as $ from 'jquery';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiService } from 'src/app/services/api.service';
import * as moment from 'moment-timezone';
import { OrderPipe } from 'ngx-order-pipe';



@Component({
  selector: 'app-contactus',
  templateUrl: './contactus.component.html',
  styleUrls: ['./contactus.component.css']
})
export class ContactusComponent implements OnInit {

  public messages: any=[];
  public spinner: any;
  public totalItems:any;
  private currentPage:any=1; // set current page to 1
  public itemsPerPage:any=10; // we are showing 10 items per page
  baseUrl: string;
  value: any;
  value1:any;
  public searchText: string = '';
  order: string = 'sentTime';
  reverse: boolean = true;

  constructor(private httpClient: HttpClient,private orderPipe: OrderPipe,private router: Router,private apiService:ApiService) { 
    
  }
  
  ngOnInit() {
    this.baseUrl=this.apiService.getBaseUrl()
    $('#idContactUsLi').addClass('active');
    this.value="";
    this.value1="";
    this.getData(this.currentPage,this.itemsPerPage,this.searchText,this.value,this.value1)
    }

    setValue() { 
      if (this.value==null){
        this.value=""
      }
      else{
        var year = this.value.getFullYear();
        var month = this.value.getMonth()+1;
        var dt = this.value.getDate();
    
        if (dt < 10) {
          var dt1 = '0' + String(dt);
        }
        else{
          var dt1=String(dt)
        }
        if (month < 10) {
          var month1 = '0' + String(month);
        }
        this.value=String(year)+'-' + month1 + '-'+dt1;
      }
      if (this.value1==null){
        this.value1=""
      }
      else{
        var year = this.value1.getFullYear();
        var month = this.value1.getMonth()+1;
        var dt = this.value1.getDate();
    
        if (dt < 10) {
          var dt1 = '0' + String(dt);
        }
        else{
          var dt1=String(dt)
        }
        if (month < 10) {
          var month1 = '0' + String(month);
        }
        this.value1=String(year)+'-' + month1 + '-'+dt1;
      }
      $('#fromDateError').text("");
      $('#toDateError').text("");

      if ((this.value!="") && (this.value1=="")){
        $('#toDateError').text("Please select to date");
        return;

      }
      if((this.value == "") && (this.value1 !="")){
        $('#fromDateError').text("Please select from date");
        return;
      }

      this.currentPage=1;
      this.getData(this.currentPage,this.itemsPerPage,this.searchText,this.value,this.value1)
     
     }

    getNext(page: any){
      if (this.value==null){
        this.value=""
      }
      else{
        var year = this.value.getFullYear();
        var month = this.value.getMonth()+1;
        var dt = this.value.getDate();
    
        if (dt < 10) {
          var dt1 = '0' + String(dt);
        }
        else{
          var dt1=String(dt)
        }
        if (month < 10) {
          var month1 = '0' + String(month);
        }
        this.value=String(year)+'-' + month1 + '-'+dt1;
      }

      this.currentPage = page;
      this.getData(this.currentPage,this.itemsPerPage,this.searchText,this.value,this.value1);
   }
    
    getData(pageNo: any,maxResults: any,searchText:any,value1:String,value2:string){

      const httpOptions = {
        headers: new HttpHeaders({
          'Authorization': localStorage.getItem('token'),
          'timeZone':moment.tz.guess()
        })
      };
    this.httpClient.post<any>(this.baseUrl+'/getContactUsMessages/',{"pageNo":pageNo,"maxRecords":maxResults,"searchText":this.searchText,"fromDate":value1,"toDate":value2},httpOptions).subscribe((res)=>{
          this.totalItems=res.count;
          this.messages=res.messages;
          this.orderPipe.transform(this.messages, this.order);

  
        },
          error  => {
        if(error.status==401){
        this.router.navigate(['']);
        };
        alert(JSON.stringify(error.error.message));
      });
  
  }
  setOrder(value: string) {
    if (this.order === value) {
      this.reverse = !this.reverse;
    }
  
    this.order = value;
  }
  
}
