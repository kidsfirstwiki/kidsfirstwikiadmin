import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RecommendationengineComponent } from './recommendationengine.component';

describe('RecommendationengineComponent', () => {
  let component: RecommendationengineComponent;
  let fixture: ComponentFixture<RecommendationengineComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RecommendationengineComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RecommendationengineComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
