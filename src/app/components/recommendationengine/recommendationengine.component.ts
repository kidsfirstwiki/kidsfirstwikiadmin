import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { NgxSmartModalService } from 'ngx-smart-modal';
import * as $ from 'jquery';
import { ApiService } from 'src/app/services/api.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';



@Component({
  selector: 'app-recommendationengine',
  templateUrl: './recommendationengine.component.html',
  styleUrls: ['./recommendationengine.component.css']
})
export class RecommendationengineComponent implements OnInit {

  baseUrl:any;
  recommendationForm: FormGroup;
  submitted = false;
  customData:any;
  recommData:any;
  
  constructor(private formBuilder: FormBuilder,private apiService:ApiService,private http: HttpClient,private router: Router) { }

  ngOnInit() {
    this.baseUrl=this.apiService.getBaseUrl();

    $('#idRecommendationEngineLi').addClass('active');

    this.recommendationForm = this.formBuilder.group({
      
      regular_article: ['',[Validators.required]],
      interest: ['',[Validators.required]],
      va5: ['',[Validators.required]],
      onThisDay: ['',[Validators.required]],
      featuredList: ['',[Validators.required]],
      featuredPicture: ['',[Validators.required]],
      history: ['',[Validators.required]],
     
  });
  this.getRecommendationData();
  
  }

  get f() { return this.recommendationForm.controls; }

  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.recommendationForm.invalid) {
        return;
    }
    this.customData=this.recommendationForm.value;
    this.apiService.editRecommendationPercentage(this.customData).subscribe(
      (res:any)=>{
      alert(res['message']);
      //this.router.navigate(['/recomm']);

    },
    (error:any)=>{

      if(error.status==401){
        this.router.navigate(['']);
        };
      alert(error.error.message);    
    }
  );




  }
  getRecommendationData(){
    this.apiService.getRecommendationPercentage().subscribe((res:any)=>{
      this.recommData=res.data;
      this.recommendationForm.controls['regular_article'].setValue(this.recommData['regular_article']);
      this.recommendationForm.controls['interest'].setValue(this.recommData['interest']);
      this.recommendationForm.controls['va5'].setValue(this.recommData['va5']);
      this.recommendationForm.controls['onThisDay'].setValue(this.recommData['onThisDay']);
      this.recommendationForm.controls['featuredPicture'].setValue(this.recommData['featuredPicture']);
      this.recommendationForm.controls['featuredList'].setValue(this.recommData['featuredList']);
      this.recommendationForm.controls['history'].setValue(this.recommData['history']);

  },
    (error:any)=>{
      alert(error.error.message);    

      if(error.status==401){
        this.router.navigate(['']);
        };
    }
  
  );


  }



}
