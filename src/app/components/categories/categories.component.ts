import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { NgxSmartModalService } from 'ngx-smart-modal';
import * as $ from 'jquery';
import { ApiService } from 'src/app/services/api.service';
import { OrderPipe } from 'ngx-order-pipe';


@Component({ 
  selector: 'app-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.css']
})
export class CategoriesComponent implements OnInit {

  public myData: any;
  public categories: any=[];
  public customData:any;
  public categoryDetail:any;
  public spinner: any;
  public totalItems:any;
  private currentPage:any=1; // set current page to 1
  public itemsPerPage:any=10; // we are showing 10 items per page
  public topic: string = '';
  public mainCategory: string = '';
  public subCategory1: string = '';
  public subCategory2: string = '';
  public subCategory3: string = '';
  public interest: string = '';
  public browseLevel1: string = '';
  public browseLevel2: string = '';
  order: string = 'jsonFile';
  reverse: boolean = false;

  
  constructor(private httpClient: HttpClient,private orderPipe: OrderPipe,private router: Router,public ngxSmartModalService: NgxSmartModalService,private apiService:ApiService) { }

  ngOnInit() {
    
    this.getData(this.currentPage,this.itemsPerPage,this.topic,this.mainCategory,this.subCategory1,this.subCategory2,this.subCategory3,this.interest,this.browseLevel1,this.browseLevel2)
    $('#idCategoriesLi').addClass('active');
  }

  setValue() { 
    this.currentPage=1;
    this.getData(this.currentPage,this.itemsPerPage,this.topic,this.mainCategory,this.subCategory1,this.subCategory2,this.subCategory3,this.interest,this.browseLevel1,this.browseLevel2)
    }
   getNext(page: any){
    this.currentPage = page;
    this.getData(this.currentPage,this.itemsPerPage,this.topic,this.mainCategory,this.subCategory1,this.subCategory2,this.subCategory3,this.interest,this.browseLevel1,this.browseLevel2)
    }
  
  getData(pageNo: any,maxResults: any,topic:string,maincat: string,subcat1:string,subcat2:string,subcat3:string,interest:string,browse1:string,browse2:string){
      this.customData={
          "pageNo":pageNo,
          "maxResults":maxResults,
          "topic":topic,
          "mainCategory":maincat,
          "subCategory1":subcat1,
          "subCategory2":subcat2,
          "subCategory3":subcat3,
          "interest":interest,
          "browseLevel1":browse1,
          "browseLevel2":browse2
      }
      this.apiService.getCategoriesData(this.customData).subscribe(
        (res:any)=>{
          this.categories=res.categories;
          this.totalItems=res.count;
          this.orderPipe.transform(this.categories, this.order);


      },
      (error:any)=>{

        if(error.status==401){
          this.router.navigate(['']);
          };
        alert(error.error.message);    
      }
    );
  }
  setOrder(value: string) {
    if (this.order === value) {
      this.reverse = !this.reverse;
    }
  
    this.order = value;
  }

}