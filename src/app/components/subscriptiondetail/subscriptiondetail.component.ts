import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-subscriptiondetail',
  templateUrl: './subscriptiondetail.component.html',
  styleUrls: ['./subscriptiondetail.component.css']
})
export class SubscriptiondetailComponent implements OnInit {
  subscriptionDetails: any;

  constructor() { }

  ngOnInit() {
    let subscriptionDetail=localStorage.getItem("subscriptionObj");
    this.subscriptionDetails=JSON.parse(subscriptionDetail);

  }

}
