import { Component, OnInit,Input } from '@angular/core';
import { ActivatedRoute, Router, ParamMap } from '@angular/router';
import { ApiService } from 'src/app/services/api.service';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import * as $ from 'jquery';


@Component({
  selector: 'app-articles',
  templateUrl: './articles.component.html',
  styleUrls: ['./articles.component.css']
})
export class ArticlesComponent implements OnInit {
  public formData: any;
  public selectedId:any;
  public totalItems: any;
  public articles: any;
  public customData:any;
  public categoryString: string;
  private currentPage:any=1; // set current page to 1
  public itemsPerPage:any=10; // we are showing 10 items per page
  public searchText: string = '';
  
  constructor(private httpClient: HttpClient,private router: Router,private _Activatedroute:ActivatedRoute,private apiService:ApiService){}
  ngOnInit() {
    
    this.selectedId=this._Activatedroute.snapshot.paramMap.get("id");
    this.getData(this.currentPage,this.itemsPerPage,this.searchText);
    
  }

  setValue() { 
    this.currentPage=1;
    this.getData(this.currentPage,this.itemsPerPage,this.searchText)
   }
   getNext(page: any){
    this.currentPage = page;
    this.getData(this.currentPage,this.itemsPerPage,this.searchText);
  }
  
  getData(pageNo: any,maxResults: any,searchText: string){
      this.customData={
          "pageNo":pageNo,
          "maxResults":maxResults,
          "searchText":searchText,
          "categoryId":this.selectedId
      }
      this.apiService.getArticlesByCategory(this.customData).subscribe(
        (res:any)=>{
          this.articles=res.articles;
          this.totalItems=res.count;
          this.categoryString=res.categoryString;

      },
      (error:any)=>{

        if(error.status==401){
          this.router.navigate(['']);
          };
        alert(error.error.message);    
      }
    );
  }

  navigateToUrl(title){
    var uri = 'https://en.wikipedia.org/api/rest_v1/page/html/'+title;
    var encoded = encodeURI(uri);  
    window.open(uri, "_blank");
  
  }



}
