import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import * as $ from 'jquery';
import { ApiService } from 'src/app/services/api.service';

@Component({
  selector: 'app-uploadcss',
  templateUrl: './uploadcss.component.html',
  styleUrls: ['./uploadcss.component.css']
})
export class UploadcssComponent implements OnInit {
  fileToUpload: File;
  uploadCSSForm: FormGroup;
  submitted = false; 
  myref:any;

  constructor(private httpClient: HttpClient,private router: Router,private apiService:ApiService,private formBuilder: FormBuilder) { }

  ngOnInit() {
      
  

    this.uploadCSSForm = this.formBuilder.group({

      fileName: ['', [Validators.required]],
  });
  let baseUrl=this.apiService.getBaseUrl()
  this.myref=baseUrl+"/media/uploadedcss/uploaded_css_file.css";
  $('#idUploadCssLi').addClass('active');

  
  }
  get f() { return this.uploadCSSForm.controls; }


  postMethod(files: FileList) {
    $('#fileErrorMsg').text("");
    this.fileToUpload = files.item(0); 
    if (!this.validateFile(this.fileToUpload.name)) {
      $('#fileErrorMsg').text('Selected file format is not supported.Please upload file in .css');
      return false;
  }
    
}
validateFile(name: String) {
  var ext = name.substring(name.lastIndexOf('.') + 1);
  if (ext.toLowerCase() == 'css') {
      return true;
  }
  else {
      return false;
  }
}

onSubmit(){
  this.submitted = true;

    // stop here if form is invalid
    if (this.uploadCSSForm.invalid) {
      $('#fileErrorMsg').text("File is required");
        return;
    }

    if (!this.validateFile(this.fileToUpload.name)) {
      $('#fileErrorMsg').text('Selected file format is not supported.Please upload file in .css');
      return false;
  }

    $("#fileErrorMsg").text("File size:"+this.fileToUpload.size+" bytes");
  let formData = new FormData(); 
    formData.append('file', this.fileToUpload, this.fileToUpload.name); 
    this.apiService.uploadCSS(formData).subscribe(
      (res:any)=>{
        alert("File uploaded successfully");
        this.router.navigate(['/uploadCss']);
    },
    (error:any)=>{

      if(error.status==401){
        this.router.navigate(['']);
        };
      alert(error.error.message);    
    }
  );
}

}
