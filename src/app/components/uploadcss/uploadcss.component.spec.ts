import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UploadcssComponent } from './uploadcss.component';

describe('UploadcssComponent', () => {
  let component: UploadcssComponent;
  let fixture: ComponentFixture<UploadcssComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UploadcssComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UploadcssComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
