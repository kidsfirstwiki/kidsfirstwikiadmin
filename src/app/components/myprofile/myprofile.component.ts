import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import * as $ from 'jquery';
import { ApiService } from 'src/app/services/api.service';


@Component({
  selector: 'app-myprofile',
  templateUrl: './myprofile.component.html',
  styleUrls: ['./myprofile.component.css']
})
export class MyprofileComponent implements OnInit {

  
  public myProfileData: any={}
  baseUrl: string;
  constructor(private httpClient: HttpClient,private router: Router,private apiService:ApiService) { }

  ngOnInit() {
    this.baseUrl=this.apiService.getBaseUrl();
    this.getProfileData()
  }


  getProfileData(){
    
    const httpOptions = {
      headers: new HttpHeaders({
        'Authorization': localStorage.getItem('token')
      })
    };
    this.httpClient.get<any>(this.baseUrl+'/getAdminProfile/',httpOptions).subscribe((res)=>{
      
      this.myProfileData=res.data;
      $('#viewProfileImage').attr("src", this.myProfileData.image);
      },
        error  => {
        
      alert(JSON.stringify(error.error.message));
      if(error.status==401){
      this.router.navigate(['']);
      };
    });

}



}
