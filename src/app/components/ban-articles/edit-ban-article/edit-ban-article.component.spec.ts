import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditBanArticleComponent } from './edit-ban-article.component';

describe('EditBanArticleComponent', () => {
  let component: EditBanArticleComponent;
  let fixture: ComponentFixture<EditBanArticleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditBanArticleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditBanArticleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
