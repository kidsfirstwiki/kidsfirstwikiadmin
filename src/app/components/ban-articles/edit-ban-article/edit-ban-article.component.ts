import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { NgxSmartModalService } from 'ngx-smart-modal';
import * as $ from 'jquery';
import { ApiService } from 'src/app/services/api.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';


@Component({
  selector: 'app-edit-ban-article',
  templateUrl: './edit-ban-article.component.html',
  styleUrls: ['./edit-ban-article.component.css']
})
export class EditBanArticleComponent implements OnInit {

  public editObj:any;
  editBanArticleForm: FormGroup;
  submitted = false;
  customData:any;

  constructor(private formBuilder: FormBuilder,private apiService:ApiService,private http: HttpClient,private router: Router) { }


  ngOnInit() {
    let editObj=localStorage.getItem("editBanArticleObj");
    this.editObj=JSON.parse(editObj);
    $('#idBanArticlesLi').addClass('active');

    this.editBanArticleForm = this.formBuilder.group({
      
      title: ['',[Validators.required]],
      
  });
  this.editBanArticleForm.controls['title'].setValue(this.editObj.title);
   }
  get f() { return this.editBanArticleForm.controls; }

  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.editBanArticleForm.invalid) {
        return;
    }
  
    this.customData=this.editBanArticleForm.value;
    this.customData['id']=this.editObj.id;
    
    this.apiService.editBanArticle(this.customData).subscribe(
      (res:any)=>{
      alert(res.message);
      this.router.navigate(['/banArticles']);
    },
    (error:any)=>{
      alert(error.error.message);
      if(error.status==401){
        this.router.navigate(['']);
        };
          
    }
  );
  }

}
