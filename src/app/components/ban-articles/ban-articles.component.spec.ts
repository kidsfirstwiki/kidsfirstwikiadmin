import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BanArticlesComponent } from './ban-articles.component';

describe('BanArticlesComponent', () => {
  let component: BanArticlesComponent;
  let fixture: ComponentFixture<BanArticlesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BanArticlesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BanArticlesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
