import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import * as $ from 'jquery';
import { ApiService } from 'src/app/services/api.service';



@Component({
  selector: 'app-upload-ban-article-csv',
  templateUrl: './upload-ban-article-csv.component.html',
  styleUrls: ['./upload-ban-article-csv.component.css']
})
export class UploadBanArticleCSVComponent implements OnInit {

  fileToUpload: File;
  uploadCSVForm: FormGroup;
  submitted = false; 
  myref:any;

  constructor(private httpClient: HttpClient,private router: Router,private apiService:ApiService,private formBuilder: FormBuilder) { }

  ngOnInit() {
    
    this.uploadCSVForm = this.formBuilder.group({

      fileName: ['', [Validators.required]],

  });
  let baseUrl=this.apiService.getBaseUrl()
  this.myref=baseUrl+"/media/files/markedBanned.xlsx";
  $('#idBanArticlesLi').addClass('active');

  
  }
  get f() { return this.uploadCSVForm.controls; }


  postMethod(files: FileList) {
    $('#fileErrorMsg').text("");
    this.fileToUpload = files.item(0); 
    if (!this.validateFile(this.fileToUpload.name)) {
      $('#fileErrorMsg').text('Selected file format is not supported.Please upload file in .xlsx');
      return false;
  }
    
}
validateFile(name: String) {
  var ext = name.substring(name.lastIndexOf('.') + 1);
  if (ext.toLowerCase() == 'xlsx') {
      return true;
  }
  else {
      return false;
  }
}

onSubmit(){
  this.submitted = true;

    // stop here if form is invalid
    if (this.uploadCSVForm.invalid) {
      $('#fileErrorMsg').text("File is required");
        return;
    }

    if (!this.validateFile(this.fileToUpload.name)) {
      $('#fileErrorMsg').text('Selected file format is not supported.Please upload file in .xlsx');
      return false;
  }

    $("#fileErrorMsg").text("File size:"+this.fileToUpload.size+" bytes");
  let formData = new FormData(); 
    formData.append('file', this.fileToUpload, this.fileToUpload.name); 
    this.apiService.uploadBanArticleCSV(formData).subscribe(
      (res:any)=>{
        alert("File uploaded successfully");
        this.router.navigate(['/banArticles']);
    },
    (error:any)=>{

      if(error.status==401){
        this.router.navigate(['']);
        };
      alert(error.error.message);    
    }
  );
}
}
