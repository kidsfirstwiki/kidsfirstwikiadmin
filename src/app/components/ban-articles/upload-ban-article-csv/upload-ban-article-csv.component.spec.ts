import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UploadBanArticleCSVComponent } from './upload-ban-article-csv.component';

describe('UploadBanArticleCSVComponent', () => {
  let component: UploadBanArticleCSVComponent;
  let fixture: ComponentFixture<UploadBanArticleCSVComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UploadBanArticleCSVComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UploadBanArticleCSVComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
