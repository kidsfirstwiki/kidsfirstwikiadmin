import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddBanArticleComponent } from './add-ban-article.component';

describe('AddBanArticleComponent', () => {
  let component: AddBanArticleComponent;
  let fixture: ComponentFixture<AddBanArticleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddBanArticleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddBanArticleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
