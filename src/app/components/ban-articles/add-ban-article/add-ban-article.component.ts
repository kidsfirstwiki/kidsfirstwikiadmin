import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { NgxSmartModalService } from 'ngx-smart-modal';
import * as $ from 'jquery';
import { ApiService } from 'src/app/services/api.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';




@Component({
  selector: 'app-add-ban-article',
  templateUrl: './add-ban-article.component.html',
  styleUrls: ['./add-ban-article.component.css']
})
export class AddBanArticleComponent implements OnInit {

  public editObj:any;
  addBanArticleForm: FormGroup;
  submitted = false;
  customData:any;
  constructor(private formBuilder: FormBuilder,private apiService:ApiService,private http: HttpClient,private router: Router) { }
  ngOnInit() {
    this.addBanArticleForm = this.formBuilder.group({
      title: ['',[Validators.required]],
      
  });
  $('#idBanArticlesLi').addClass('active');

  }

  get f() { return this.addBanArticleForm.controls; }

  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.addBanArticleForm.invalid) {
        return;
    }
  
    this.customData=this.addBanArticleForm.value;
    this.apiService.addBanArticle(this.customData).subscribe(
      (res:any)=>{
      alert(res['message']);
      this.router.navigate(['/banArticles']);

    },
    (error:any)=>{

      if(error.status==401){
        this.router.navigate(['']);
        };
      alert(error.error.message);    
    }
  );

  }
}
