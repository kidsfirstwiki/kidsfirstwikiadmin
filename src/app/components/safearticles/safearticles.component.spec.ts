import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SafearticlesComponent } from './safearticles.component';

describe('SafearticlesComponent', () => {
  let component: SafearticlesComponent;
  let fixture: ComponentFixture<SafearticlesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SafearticlesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SafearticlesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
