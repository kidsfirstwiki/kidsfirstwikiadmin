import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditsafearticleComponent } from './editsafearticle.component';

describe('EditsafearticleComponent', () => {
  let component: EditsafearticleComponent;
  let fixture: ComponentFixture<EditsafearticleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditsafearticleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditsafearticleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
