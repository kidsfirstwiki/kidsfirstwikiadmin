import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { NgxSmartModalService } from 'ngx-smart-modal';
import * as $ from 'jquery';
import { ApiService } from 'src/app/services/api.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-editsafearticle',
  templateUrl: './editsafearticle.component.html',
  styleUrls: ['./editsafearticle.component.css']
})
export class EditsafearticleComponent implements OnInit {

  public editObj:any;
  editSafeArticleForm: FormGroup;
  submitted = false;
  customData:any;

  constructor(private formBuilder: FormBuilder,private apiService:ApiService,private http: HttpClient,private router: Router) { }


  ngOnInit() {
    let editObj=localStorage.getItem("editSafeArticleObj");
    this.editObj=JSON.parse(editObj);
    $('#idSafeArticlesLi').addClass('active');

    this.editSafeArticleForm = this.formBuilder.group({
      
      title: ['',[Validators.required]],
      
  });
  this.editSafeArticleForm.controls['title'].setValue(this.editObj.title);
   }
  get f() { return this.editSafeArticleForm.controls; }

  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.editSafeArticleForm.invalid) {
        return;
    }
  
    this.customData=this.editSafeArticleForm.value;
    this.customData['id']=this.editObj.id;
    
    this.apiService.editSafeArticle(this.customData).subscribe(
      (res:any)=>{
      alert(res.message);
      this.router.navigate(['/safeArticles']);
    },
    (error:any)=>{
      alert(error.error.message);
      if(error.status==401){
        this.router.navigate(['']);
        };
          
    }
  );
  }

}
