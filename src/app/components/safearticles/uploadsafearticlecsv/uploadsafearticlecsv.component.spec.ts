import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UploadsafearticlecsvComponent } from './uploadsafearticlecsv.component';

describe('UploadsafearticlecsvComponent', () => {
  let component: UploadsafearticlecsvComponent;
  let fixture: ComponentFixture<UploadsafearticlecsvComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UploadsafearticlecsvComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UploadsafearticlecsvComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
