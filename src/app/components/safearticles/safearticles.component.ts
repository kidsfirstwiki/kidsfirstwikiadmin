import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { NgxSmartModalService } from 'ngx-smart-modal';
import * as $ from 'jquery';
import { ApiService } from 'src/app/services/api.service';
import { OrderPipe } from 'ngx-order-pipe';


@Component({
  selector: 'app-safearticles',
  templateUrl: './safearticles.component.html',
  styleUrls: ['./safearticles.component.css']
})
export class SafearticlesComponent implements OnInit {

  public myData: any;
  public safeArticlesList: any;
  public customData:any;
  public spinner: any;
  public totalItems:any;
  private currentPage:any=1; // set current page to 1
  public itemsPerPage:any=10; // we are showing 10 items per page
  public searchText: string = '';
  order: string = 'title';
  reverse: boolean = false; 
  constructor(private httpClient: HttpClient,private router: Router,private orderPipe: OrderPipe,public ngxSmartModalService: NgxSmartModalService,private apiService:ApiService) { }

  ngOnInit() {
    this.getData(this.currentPage,this.itemsPerPage,this.searchText)
    $('#idSafeArticlesLi').addClass('active');
  }
  setValue() { 
    this.currentPage=1;
    this.getData(this.currentPage,this.itemsPerPage,this.searchText)
   }
   getNext(page: any){
    this.currentPage = page;
    this.getData(this.currentPage,this.itemsPerPage,this.searchText);
  }
  
  getData(pageNo: any,maxResults: any,searchText: string){
    
      this.apiService.getSafeArticlesList(pageNo,maxResults,searchText).subscribe(
        (res:any)=>{
          this.safeArticlesList=res.list;
          this.totalItems=res.count;
          this.orderPipe.transform(this.safeArticlesList, this.order);


      },
      (error:any)=>{

        if(error.status==401){
          this.router.navigate(['']);
          };
        alert(error.error.message);    
      }
    );
  }
 
  // onView(obj:object){
  //   localStorage.setItem("viewSafeArticleObj",JSON.stringify(obj))
  //   this.router.navigate(['/safeArticles/view']);
  // }


  navigateToUrl(title){
    var uri = 'https://en.wikipedia.org/api/rest_v1/page/html/'+title;
    var encoded = encodeURI(uri);  
    window.open(uri, "_blank");
  
  }

  onEdit(obj:object){
    localStorage.setItem("editSafeArticleObj",JSON.stringify(obj))
    this.router.navigate(['/safeArticles/edit']);

  }
  onDelete(obj:Object){
    var result = confirm("Are you sure,you really want to delete this article?"); 
    if (result == true) { 
        let formData={"id":obj['id']}
        this.apiService.deleteSafeArticle(formData).subscribe(
          (res:any)=>{
            var index = -1;
            for(var i = 0; i < this.safeArticlesList.length; i++){
              if(this.safeArticlesList[i].id == obj['id']){
                index = i;
                break;
              }
            }
            if(index != -1){
              this.safeArticlesList.splice(index, 1);
            }
            alert("Deleted successfully")
        },
        (error:any)=>{
          
          if(error.status==401){
            this.router.navigate(['']);
            };
          alert(error.error.message);    
        }
      );
    } 
    
  }
  setOrder(value: string) {
    if (this.order === value) {
      this.reverse = !this.reverse;
    }
  
    this.order = value;
  }
  exportCSV() {

    this.apiService.exportCSVForSafeTitles().subscribe(
          (res:any)=>{
           var uri=res.data
           var uri =res.fileUrl ;
           window.open(uri, "_blank");
  
        },
        (error:any)=>{
  
          if(error.status==401){
            this.router.navigate(['']);
            };
          alert(error.error.message);    
        }
      );
     
     }

}
