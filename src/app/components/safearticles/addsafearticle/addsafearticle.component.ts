import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { NgxSmartModalService } from 'ngx-smart-modal';
import * as $ from 'jquery';
import { ApiService } from 'src/app/services/api.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';


@Component({
  selector: 'app-addsafearticle',
  templateUrl: './addsafearticle.component.html',
  styleUrls: ['./addsafearticle.component.css']
})
export class AddsafearticleComponent implements OnInit {

  public editObj:any;
  addSafeArticleForm: FormGroup;
  submitted = false;
  customData:any;
  constructor(private formBuilder: FormBuilder,private apiService:ApiService,private http: HttpClient,private router: Router) { }
  ngOnInit() {
    this.addSafeArticleForm = this.formBuilder.group({
      title: ['',[Validators.required]],
      
  });
  $('#idSafeArticlesLi').addClass('active');

  }

  get f() { return this.addSafeArticleForm.controls; }

  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.addSafeArticleForm.invalid) {
        return;
    }
  
    this.customData=this.addSafeArticleForm.value;
    this.apiService.addSafeArticle(this.customData).subscribe(
      (res:any)=>{
      alert(res['message']);
      this.router.navigate(['/safeArticles']);

    },
    (error:any)=>{

      if(error.status==401){
        this.router.navigate(['']);
        };
      alert(error.error.message);    
    }
  );

  }

}
