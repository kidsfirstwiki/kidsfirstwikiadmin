import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddsafearticleComponent } from './addsafearticle.component';

describe('AddsafearticleComponent', () => {
  let component: AddsafearticleComponent;
  let fixture: ComponentFixture<AddsafearticleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddsafearticleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddsafearticleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
