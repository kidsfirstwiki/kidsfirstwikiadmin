import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { NgxSmartModalService } from 'ngx-smart-modal';
import { OrderPipe } from 'ngx-order-pipe';

import * as $ from 'jquery';
import { ApiService } from 'src/app/services/api.service';
import * as moment from 'moment-timezone';
import { from } from 'rxjs';

@Component({
  selector:'app-dashboard', 
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
 
  
  public myData: any={};
  public users: any=[];
  public userDetail:any={};
  public childDetail:any=[];
  public spinner: any;
  public totalItems:any;
  private currentPage:any=1; // set current page to 1
  public itemsPerPage:any=10; // we are showing 10 items per page
  public searchText: string = '';
  public dateJoined: string = '';
  order: string = 'date_joined';
  reverse: boolean = true;

  subscriptionDetails: any;
  userName:string;
  baseUrl: any;
  value: any;
  value1:any;
  fromDate:any;
  toDate:any;
  public date: Object = new Date()
  formData:{}
  status: number;
  constructor(private httpClient: HttpClient,private orderPipe: OrderPipe,private router: Router,public ngxSmartModalService: NgxSmartModalService,private apiService:ApiService) { 
    
  }

  ngOnInit() {
    this.baseUrl=this.apiService.getBaseUrl()
    this.value=null;
    this.value1=null;
    this.fromDate="";
    this.toDate="";
    this.getData(this.currentPage,this.itemsPerPage,this.searchText,this.fromDate,this.toDate)
    $('#idDashboardLi').addClass('active');
  }

   openMyModal(user:any){
    this.userDetail=user;
    this.childDetail=user['childs'];
    this.ngxSmartModalService.getModal('myModal').open() 
   }

   showSubscription(obj:any,name:string){
    this.subscriptionDetails=obj;
    this.userName=name;
    this.ngxSmartModalService.getModal('subscriptionModal').open() 

   }

   setValue() { 
    if (this.value==null){
      this.fromDate=""
    }
    else{
      var year = this.value.getFullYear();
      var month = this.value.getMonth()+1;
      var dt = this.value.getDate();
  
      if (dt < 10) {
        var dt1 = '0' + String(dt);
      }
      else{
        var dt1=String(dt)
      }
      if (month < 10) {
        var month1 = '0' + String(month);
      }
      this.fromDate=String(year)+'-' + month1 + '-'+dt1;
    }
    if (this.value1==null){
      this.toDate=""
    }
    else{
      var year = this.value1.getFullYear();
      var month = this.value1.getMonth()+1;
      var dt = this.value1.getDate();
  
      if (dt < 10) {
        var dt1 = '0' + String(dt);
      }
      else{
        var dt1=String(dt)
      }
      if (month < 10) {
        var month1 = '0' + String(month);
      }
      this.toDate=String(year)+'-' + month1 + '-'+dt1;
    }
    $('#fromDateError').text("");
    $('#toDateError').text("");

    if ((this.fromDate!="") && (this.toDate=="")){
      $('#toDateError').text("Please select to date");
      return;

    }
    if((this.fromDate == "") && (this.toDate !="")){
      $('#fromDateError').text("Please select from date");
      return;
    }

    this.currentPage=1;
    this.getData(this.currentPage,this.itemsPerPage,this.searchText,this.fromDate,this.toDate)
   
   }


//   getNext(page: any){
//     this.currentPage = page;
//     if (this.value==null){
//       this.value=""
//     }
//     else{
//       var year = this.value.getFullYear();
//       var month = this.value.getMonth()+1;
//       var dt = this.value.getDate();
  
//       if (dt < 10) {
//         var dt1 = '0' + String(dt);
//       }
//       else{
//         var dt1=String(dt)
//       }
//       if (month < 10) {
//         var month1 = '0' + String(month);
//       }
//       this.value=String(year)+'-' + month1 + '-'+dt1;
//     }

//     this.getData(this.currentPage,this.itemsPerPage,this.searchText,this.fromDate,this.toDate);
//  }
  
  getData(pageNo: any,maxResults: any,searchText: string,from:string,to:string){
    
    const httpOptions = {
      headers: new HttpHeaders({
        'Authorization': localStorage.getItem('token'),
        'timeZone':moment.tz.guess()
      })
    };
    this.formData={"pageNo":pageNo,"maxRecords":maxResults,"searchText":searchText,"fromDate":from,"toDate":to}
  this.httpClient.post<any>(this.baseUrl+'/getDashboardData/',this.formData,httpOptions).subscribe((res)=>{
        this.myData=res.data;
        this.totalItems=res.count;
        this.users=res.users;
        this.orderPipe.transform(this.users, this.order);
        

      },
        error  => {
      if(error.status==401){
      this.router.navigate(['']);
      };
      alert(JSON.stringify(error.error.message));
    });

}
setValue1() { 
  if (this.value==null){
    this.fromDate=""
  }
  else{
    var year = this.value.getFullYear();
    var month = this.value.getMonth()+1;
    var dt = this.value.getDate();

    if (dt < 10) {
      var dt1 = '0' + String(dt);
    }
    else{
      var dt1=String(dt)
    }
    if (month < 10) {
      var month1 = '0' + String(month);
    }
    this.fromDate=String(year)+'-' + month1 + '-'+dt1;
  }
  if (this.value1==null){
    this.toDate=""
  }
  else{
    var year = this.value1.getFullYear();
    var month = this.value1.getMonth()+1;
    var dt = this.value1.getDate();

    if (dt < 10) {
      var dt1 = '0' + String(dt);
    }
    else{
      var dt1=String(dt)
    }
    if (month < 10) {
      var month1 = '0' + String(month);
    }
    this.toDate=String(year)+'-' + month1 + '-'+dt1;
  }
  $('#fromDateError').text("");
  $('#toDateError').text("");

  if ((this.fromDate!="") && (this.toDate=="")){
    $('#toDateError').text("Please select to date");
    return;

  }
  if((this.fromDate == "") && (this.toDate !="")){
    $('#fromDateError').text("Please select from date");
    return;
  }

  this.currentPage=1;
  this.exportCSV(this.searchText,this.fromDate,this.toDate)
 
 }





exportCSV(searchText: string,from:string,to:string){

  const httpOptions = {
    headers: new HttpHeaders({
      'Authorization': localStorage.getItem('token'),
      'timeZone':moment.tz.guess()
    })
  };
  this.formData={"searchText":searchText,"fromDate":from,"toDate":to}
this.httpClient.post<any>(this.baseUrl+'/exportCSVForUsers/',this.formData,httpOptions).subscribe((res)=>{
       var uri =res.fileUrl ;
       window.open(uri, "_blank");
      

    },
      error  => {
    if(error.status==401){
    this.router.navigate(['']);
    };
    alert(JSON.stringify(error.error.message));
  });

}


editUser(user:object){
  localStorage.setItem("editUserObj",JSON.stringify(user))
    this.router.navigate(['/dashboard/editUserDetail']);
}

setOrder(value: string) {
  if (this.order === value) {
    this.reverse = !this.reverse;
  }

  this.order = value;
}

checkBoxClick(userObj:any){
  if($('#isButtonSelected').is(":checked")){
  this.status=1;
  }
  else{
    this.status=0;
  }
  const httpOptions = {
    headers: new HttpHeaders({
      'Authorization': localStorage.getItem('token'),
      'timeZone':moment.tz.guess()
    })
  };
  
  this.formData={"status":this.status,"userId":userObj.id}
  this.httpClient.post<any>(this.baseUrl+'/setOrUnsetSubscription/',this.formData,httpOptions).subscribe((res)=>{
        
      alert(res.message);
      this.fromDate="";
      this.toDate="";
      this.getData(this.currentPage,this.itemsPerPage,this.searchText,this.fromDate,this.toDate)
    
      },
        error  => {
      if(error.status==401){
      this.router.navigate(['']);
      };
      alert(JSON.stringify(error.error.message));
    });

  

}

  
}
