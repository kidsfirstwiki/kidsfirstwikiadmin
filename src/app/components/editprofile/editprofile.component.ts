import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {DomSanitizer} from '@angular/platform-browser';
import {Component, NgModule, VERSION, Input, OnInit} from '@angular/core'
import {BrowserModule} from '@angular/platform-browser'
import * as $ from 'jquery';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiService } from 'src/app/services/api.service';




@Component({
  selector: 'app-editprofile',
  templateUrl: './editprofile.component.html',
  styleUrls: ['./editprofile.component.css']
})
export class EditprofileComponent implements OnInit {

  editProfileForm: FormGroup;
  submitted = false;
  myString: string;
  myProfileData: any;
  constructor(private formBuilder: FormBuilder,private apiService:ApiService,private http: HttpClient,private sanitizer: DomSanitizer,private router: Router) { }
  fileName: string;
  filePreview: string;
  customData:any;


  ngOnInit() {
    
    this.filePreview="";
    this.getProfileData()

    this.editProfileForm = this.formBuilder.group({
      
      email: ['', [Validators.required]],
      firstName: ['', [Validators.required]],
      lastName: ['', [Validators.required]],
      address: ['', [Validators.required]],
      image:[''],

  });
  
  }
  get f() { return this.editProfileForm.controls; }

  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.editProfileForm.invalid) {
        return;
    }
  
    this.customData=this.editProfileForm.value;
    this.customData['profile_pic']=this.filePreview;
    this.apiService.editAdminProfile(this.customData).subscribe(
      (res:any)=>{
      alert(res['message']);
      this.router.navigate(['/myprofile']);

    },
    (error:any)=>{

      if(error.status==401){
        this.router.navigate(['']);
        };
      alert(error.error.message);    
    }
  );


  }

  getProfileData(){
      
    this.apiService.getAdminProfile().subscribe((res:any)=>{
      this.myProfileData=res.data;
      this.editProfileForm.controls['firstName'].setValue(this.myProfileData['firstName']);
      this.editProfileForm.controls['lastName'].setValue(this.myProfileData['lastName']);
      this.editProfileForm.controls['email'].setValue(this.myProfileData['email']);
      this.editProfileForm.controls['address'].setValue(this.myProfileData['address']);
      $('#viewProfileImage').attr("src", this.myProfileData.image);

  },
    (error:any)=>{
      alert(error.error.message);    

      if(error.status==401){
        this.router.navigate(['']);
        };
    }
  
  );

  }


  onFileChanged(event:any) {

    let reader = new FileReader();
    if (event.target.files && event.target.files.length > 0) {
      let file = event.target.files[0];
      reader.readAsDataURL(file);
      reader.onload = () => {
         this.myString=(<string> reader.result).split(',')[1];
        this.fileName = file.name + " " + file.type;
        this.filePreview = 'data:image/png' + ';base64,' + this.myString;
        $('#viewProfileImage').attr("src", this.filePreview);


      };
    }
  }
  
  sanitize(url: string) {
    //return url;
    return this.sanitizer.bypassSecurityTrustUrl(url);
  } 

  


  

}

  
