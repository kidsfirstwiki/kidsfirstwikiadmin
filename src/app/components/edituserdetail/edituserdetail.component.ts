import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { NgxSmartModalService } from 'ngx-smart-modal';
import * as $ from 'jquery';
import { ApiService } from 'src/app/services/api.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';


@Component({
  selector: 'app-edituserdetail',
  templateUrl: './edituserdetail.component.html',
  styleUrls: ['./edituserdetail.component.css']
})
export class EdituserdetailComponent implements OnInit {
  editUserObj: any;
  editUserDetailForm: FormGroup;
  submitted = false;
  customData: any;

  constructor(private formBuilder: FormBuilder,private apiService:ApiService,private http: HttpClient,private router: Router) {
    

   }

  ngOnInit() {
    let editUserObj=localStorage.getItem("editUserObj");
    this.editUserObj=JSON.parse(editUserObj);
    //alert(JSON.stringify(this.editUserObj));
    $('#idDashboardLi').addClass('active');
    this.editUserDetailForm = this.formBuilder.group({
      
      firstName: ['',[Validators.required]],
      lastName: ['',[Validators.required]],
      email: ['',[Validators.required]],
      address: ['',[Validators.required]],
    });
    this.editUserDetailForm.controls['firstName'].setValue(this.editUserObj.firstName);
    this.editUserDetailForm.controls['lastName'].setValue(this.editUserObj.lastName);
    this.editUserDetailForm.controls['email'].setValue(this.editUserObj.email);
    this.editUserDetailForm.controls['address'].setValue(this.editUserObj.address);
  }

  get f() { return this.editUserDetailForm.controls; }

  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.editUserDetailForm.invalid) {
        return;
    }
  
    this.customData=this.editUserDetailForm.value;
    this.customData['id']=this.editUserObj.id;

    
    this.apiService.editUserDetail(this.customData).subscribe(
      (res:any)=>{
      alert(res.message);
      this.router.navigate(['/dashboard']);
    },
    (error:any)=>{
      alert(error.error.message);
      if(error.status==401){
        this.router.navigate(['']);
        };
          
    }
  );

  }

}
