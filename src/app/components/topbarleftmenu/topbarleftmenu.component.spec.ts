import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TopbarleftmenuComponent } from './topbarleftmenu.component';

describe('TopbarleftmenuComponent', () => {
  let component: TopbarleftmenuComponent;
  let fixture: ComponentFixture<TopbarleftmenuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TopbarleftmenuComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TopbarleftmenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
