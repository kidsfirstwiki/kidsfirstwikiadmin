import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiService } from 'src/app/services/api.service';



@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  submitted = false;
  baseUrl: string;


  constructor(private formBuilder: FormBuilder,private httpClient: HttpClient,private router: Router,private apiService:ApiService) { }

  ngOnInit() {

    this.baseUrl=this.apiService.getBaseUrl()
    
    this.loginForm = this.formBuilder.group({
      
      email: ['', [Validators.required,Validators.compose([
        Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$")
     ])]],
      password: ['', [Validators.required]]
  });
  }

  get f() { return this.loginForm.controls; }

  onSubmit() {
      this.submitted = true;

      // stop here if form is invalid
      if (this.loginForm.invalid) {
          
          return;
      }
      // alert('SUCCESS!! :-)'+JSON.stringify(this.loginForm.value))
      
      this.httpClient.post<any>(this.baseUrl+"/loginWikiAdmin/",this.loginForm.value)
      .subscribe(
        data  => {
          var token=data.token;
          localStorage.setItem('token', token);
          this.router.navigate(['dashboard']);

        },
        error  => {
          if(error.status==401){
          };
          alert(JSON.stringify(error.error.message));
          console.log("Error", error);

        }
      );
    
    }






      }

  
  



