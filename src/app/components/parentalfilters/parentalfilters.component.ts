import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { NgxSmartModalService } from 'ngx-smart-modal';
import * as $ from 'jquery';
import { ApiService } from 'src/app/services/api.service';
import { OrderPipe } from 'ngx-order-pipe';


@Component({
  selector: 'app-parentalfilters',
  templateUrl: './parentalfilters.component.html',
  styleUrls: ['./parentalfilters.component.css']
})
export class ParentalfiltersComponent implements OnInit {

  public myData: any;
  public parentalList: any;
  public customData:any;
  public spinner: any;
  public totalItems:any;
  private currentPage:any=1; // set current page to 1
  public itemsPerPage:any=10; // we are showing 10 items per page
  public filterTopic: string = '';
  public checkTitle: string = '';
  public checkBody: string = '';
  public replaceWord: string = '';
  order: string = 'filtertopic';
  reverse: boolean = false;
  
  constructor(private httpClient: HttpClient,private router: Router,private orderPipe: OrderPipe,public ngxSmartModalService: NgxSmartModalService,private apiService:ApiService) { }

  ngOnInit() {
    
    this.getData(this.currentPage,this.itemsPerPage,this.filterTopic,this.checkTitle,this.checkBody,this.replaceWord)
    $('#idParentalFiltersLi').addClass('active');
  }

  setValue() { 
    this.currentPage=1;
    this.getData(this.currentPage,this.itemsPerPage,this.filterTopic,this.checkTitle,this.checkBody,this.replaceWord)
   }
   getNext(page: any){
    this.currentPage = page;
    this.getData(this.currentPage,this.itemsPerPage,this.filterTopic,this.checkTitle,this.checkBody,this.replaceWord);
  }
  
  getData(pageNo: any,maxResults: any,filtertopic: string,checktitle:string,checkbody:string,replaceword:string){
    
      this.apiService.getParentalFiltersData(pageNo,maxResults,filtertopic,checktitle,checkbody,replaceword).subscribe(
        (res:any)=>{
          this.parentalList=res.list;
          this.totalItems=res.count;
          this.orderPipe.transform(this.parentalList, this.order);


      },
      (error:any)=>{

        if(error.status==401){
          this.router.navigate(['']);
          };
        alert(error.error.message);    
      }
    );
  }
 
  onView(obj:object){
    localStorage.setItem("parentalViewObj",JSON.stringify(obj))
    this.router.navigate(['/parentalfilters/view']);
  }

  onEdit(obj:object){
    localStorage.setItem("parentalEditObj",JSON.stringify(obj))
    this.router.navigate(['/parentalfilters/edit']);

  }
  onDelete(obj:Object){
    var result = confirm("Are you sure,you really want to delete this filter?"); 
    if (result == true) { 
        let formData={"id":obj['id']}
        this.apiService.deleteParentalFilter(formData).subscribe(
          (res:any)=>{
            var index = -1;
            for(var i = 0; i < this.parentalList.length; i++){
              if(this.parentalList[i].id == obj['id']){
                index = i;
                break;
              }
            }
            if(index != -1){
              this.parentalList.splice(index, 1);
            }
            alert("Deleted successfully")
        },
        (error:any)=>{
          
                  if(error.status==401){
                    this.router.navigate(['']);
                    };
                  alert(error.error.message);    
                }
              );
                

            } 
    
  }

  setOrder(value: string) {
    if (this.order === value) {
      this.reverse = !this.reverse;
    }
  
    this.order = value;
  }
  exportCSV() {

    this.apiService.exportCSVForParentalFilters().subscribe(
          (res:any)=>{
           var uri=res.data
           var uri =res.fileUrl ;
           window.open(uri, "_blank");
  
        },
        (error:any)=>{
  
          if(error.status==401){
            this.router.navigate(['']);
            };
          alert(error.error.message);    
        }
      );
     
     }
  

}
 