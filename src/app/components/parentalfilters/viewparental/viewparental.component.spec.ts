import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewparentalComponent } from './viewparental.component';

describe('ViewparentalComponent', () => {
  let component: ViewparentalComponent;
  let fixture: ComponentFixture<ViewparentalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewparentalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewparentalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
