import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-viewparental',
  templateUrl: './viewparental.component.html',
  styleUrls: ['./viewparental.component.css']
})
export class ViewparentalComponent implements OnInit {

  public viewObj:any;
  constructor() { }

  ngOnInit() {
    let viewObj=localStorage.getItem("parentalViewObj");
    this.viewObj=JSON.parse(viewObj);
    $('#idParentalFiltersLi').addClass('active');


  }

}
