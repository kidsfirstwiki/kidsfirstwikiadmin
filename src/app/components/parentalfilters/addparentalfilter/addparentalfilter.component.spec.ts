import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddparentalfilterComponent } from './addparentalfilter.component';

describe('AddparentalfilterComponent', () => {
  let component: AddparentalfilterComponent;
  let fixture: ComponentFixture<AddparentalfilterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddparentalfilterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddparentalfilterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
