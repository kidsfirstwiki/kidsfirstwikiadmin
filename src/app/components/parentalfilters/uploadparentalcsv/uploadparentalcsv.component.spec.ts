import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UploadparentalcsvComponent } from './uploadparentalcsv.component';

describe('UploadparentalcsvComponent', () => {
  let component: UploadparentalcsvComponent;
  let fixture: ComponentFixture<UploadparentalcsvComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UploadparentalcsvComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UploadparentalcsvComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
