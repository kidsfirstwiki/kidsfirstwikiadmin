import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { NgxSmartModalService } from 'ngx-smart-modal';
import * as $ from 'jquery';
import { ApiService } from 'src/app/services/api.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';


@Component({
  selector: 'app-editparental',
  templateUrl: './editparental.component.html',
  styleUrls: ['./editparental.component.css']
})
export class EditparentalComponent implements OnInit {

  public editObj:any;
  editFilterForm: FormGroup;
  submitted = false;
  customData:any;

  constructor(private formBuilder: FormBuilder,private apiService:ApiService,private http: HttpClient,private router: Router) { }


  ngOnInit() {
    let editObj=localStorage.getItem("parentalEditObj");
    this.editObj=JSON.parse(editObj);
    $('#idParentalFiltersLi').addClass('active');

    this.editFilterForm = this.formBuilder.group({
      
      filterTopic: ['',[Validators.required]],
      checkTitle: ['',[Validators.required]],
      checkBody: [''],
      replaceWord: [''],
  });
  this.editFilterForm.controls['filterTopic'].setValue(this.editObj.filtertopic);
  this.editFilterForm.controls['checkTitle'].setValue(this.editObj.checktitle);
  this.editFilterForm.controls['checkBody'].setValue(this.editObj.checkbody);
  this.editFilterForm.controls['replaceWord'].setValue(this.editObj.replaceword);

  }
  get f() { return this.editFilterForm.controls; }

  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.editFilterForm.invalid) {
        return;
    }
  
    this.customData=this.editFilterForm.value;
    this.customData['id']=this.editObj.id;
    
    this.apiService.editParentalFilters(this.customData).subscribe(
      (res:any)=>{
      alert(res.message);
      this.router.navigate(['/parentalfilters']);
    },
    (error:any)=>{
      alert(error.error.message);
      if(error.status==401){
        this.router.navigate(['']);
        };
          
    }
  );

  }

}
