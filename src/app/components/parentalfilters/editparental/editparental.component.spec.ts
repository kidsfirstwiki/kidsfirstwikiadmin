import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditparentalComponent } from './editparental.component';

describe('EditparentalComponent', () => {
  let component: EditparentalComponent;
  let fixture: ComponentFixture<EditparentalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditparentalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditparentalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
