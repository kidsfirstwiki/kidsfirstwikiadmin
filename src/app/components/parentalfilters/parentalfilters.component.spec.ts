import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ParentalfiltersComponent } from './parentalfilters.component';

describe('ParentalfiltersComponent', () => {
  let component: ParentalfiltersComponent;
  let fixture: ComponentFixture<ParentalfiltersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ParentalfiltersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ParentalfiltersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
